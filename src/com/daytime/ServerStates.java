package com.daytime;

public interface ServerStates {
	static final int STARTED = 0, BY_USER = 1, PORT_CONFLICT = 2;
}
