package com.daytime;

public interface ServerListener extends ServerStates {
	void stateChange(int iCause, int iPort);
}
