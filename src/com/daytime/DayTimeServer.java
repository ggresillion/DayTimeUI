package com.daytime;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;

public class DayTimeServer {
	protected Service _service = new Service();

	public void addServerListener( ServerListener lsn ) {
		_service.addServerListener(lsn);
	}
	public void removeServerListener( ServerListener lsn ) {
		_service.removeServerListener(lsn);
	}
	public void start(int iPort) {
		_service.start(iPort);
	}
	public void stop() {
		_service.stop();
	}
	protected class Service implements Runnable, ServerStates {
		protected volatile int port = 13;
		protected ServerSocket _sockSvr;
		protected Thread _thr;
		protected ServerListener _lsn;

		public void addServerListener( ServerListener lsn ) {
			// lsn also used from run method thread
			synchronized (this) {
				if (_lsn == null)
					_lsn = lsn;
			}
		}
		public void removeServerListener( ServerListener lsn ) {
			// lsn also used from run method thread
			synchronized (this) {
				if (_lsn == lsn)
					_lsn = null;
			}
		}
		public void start(int iPort) {
			synchronized (this) {
				if (_thr == null) {
					this.port = iPort;
					_thr=new Thread(_service);
					_thr.setDaemon(true); // exits if no non-daemon threads remain
					_thr.start();
				}
			}
		}
		public void stop() {
			try {
				_sockSvr.close();
			} catch (Exception e) {
			}
		}
		public void run() {
			try {
				DateFormat formater = DateFormat.getDateTimeInstance(
						DateFormat.LONG, DateFormat.LONG);
				notifyStateChange(STARTED, port);
				_sockSvr = new ServerSocket(port);
				try {
					while (true) {
						Socket sock = _sockSvr.accept();
						String strTime = formater.format(new Date());
						OutputStream os = sock.getOutputStream();
						PrintWriter pw = new PrintWriter(os, true);
						pw.println(strTime);
						pw.close();
						sock.close();
					}
				} catch (IOException e) {
					notifyStateChange(BY_USER, port);
				}
			} catch (IOException e) {
				notifyStateChange(PORT_CONFLICT, port);
			}
			synchronized (this) {
				_thr = null;
			}
		}
		protected void notifyStateChange(int iCause, int iPort) {
			// lsn also used from thread calling add/removeServerListener
			synchronized (this) {
				if (_lsn != null)
					_lsn.stateChange(iCause, iPort);
			}
		}
	}
}
