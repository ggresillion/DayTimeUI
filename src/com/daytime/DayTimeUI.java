package com.daytime;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.SelectionAdapter;

public class DayTimeUI extends Shell {
	private Text txtPort;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Display display = Display.getDefault();
			DayTimeUI shell = new DayTimeUI(display);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the shell.
	 * 
	 * @param display
	 */
	public DayTimeUI(Display display) {
		super(display, SWT.SHELL_TRIM);
		setLayout(new FormLayout());

		// Menu Bar
		Menu mbMain = new Menu(this, SWT.BAR);
		setMenuBar(mbMain);

		MenuItem miFile = new MenuItem(mbMain, SWT.CASCADE);
		miFile.setText("&File");

		Menu menu = new Menu(miFile);
		miFile.setMenu(menu);

		MenuItem miExit = new MenuItem(menu, SWT.NONE);
		miExit.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				System.out.print("Application Closed");
				close();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {

			}
		});
		miExit.setText("E&xit");

		MenuItem miServer = new MenuItem(mbMain, SWT.CASCADE);
		miServer.setText("&Server");

		Menu menu_1 = new Menu(miServer);
		miServer.setMenu(menu_1);

		MenuItem mntmStart = new MenuItem(menu_1, SWT.NONE);
		mntmStart.setText("St&art");

		MenuItem mntmStop = new MenuItem(menu_1, SWT.NONE);
		mntmStop.setText("St&op");
		
		//Start button

		CoolBar clbMain = new CoolBar(this, SWT.NONE);
		clbMain.setLayoutData(new FormData());

		CoolItem cliMain = new CoolItem(clbMain, SWT.NONE);

		ToolBar tlbMain = new ToolBar(clbMain, SWT.FLAT | SWT.RIGHT);
		cliMain.setControl(tlbMain);

		ToolItem tiStart = new ToolItem(tlbMain, SWT.NONE);
		tiStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("Server Started");
			}
		});
		tiStart.setText("Start");

		//Group
		
		Group grpServer = new Group(this, SWT.NONE);
		FormData fd_grpServer = new FormData();
		fd_grpServer.top = new FormAttachment(50);
		fd_grpServer.left = new FormAttachment(50);
		Point ptSize = grpServer.getSize();
		grpServer.pack();
		fd_grpServer.top.offset = -ptSize.y / 2;
		fd_grpServer.left.offset = -ptSize.x / 2;

		grpServer.setLayoutData(fd_grpServer);
		grpServer.setText("Set the service port :");
		grpServer.setLayout(new RowLayout(SWT.HORIZONTAL));

		Label lblService = new Label(grpServer, SWT.NONE);
		lblService.setText("Service Port");

		txtPort = new Text(grpServer, SWT.BORDER);
		txtPort.setText("13");

		//Status Bar
		CoolBar clbStatus = new CoolBar(this, SWT.FLAT);
		FormData fd_clbStatus = new FormData();
		fd_clbStatus.bottom = new FormAttachment(100);
		fd_clbStatus.left = new FormAttachment(0);
		clbStatus.setLayoutData(fd_clbStatus);

		CoolItem cliStatus = new CoolItem(clbStatus, SWT.NONE);

		Composite cmpStatus = new Composite(clbStatus, SWT.NONE);
		cliStatus.setControl(cmpStatus);
		cmpStatus.setLayout(new GridLayout(3, false));

		Label lblLeft = new Label(cmpStatus, SWT.NONE);
		GridData gd_lblLeft = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblLeft.heightHint = 15;
		lblLeft.setLayoutData(gd_lblLeft);
		lblLeft.setText("Stopped.");

		Label lblSep = new Label(cmpStatus, SWT.SEPARATOR);
		GridData gd_lblSep = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblSep.heightHint = 15;
		lblSep.setLayoutData(gd_lblSep);

		Label lblRight = new Label(cmpStatus, SWT.NONE);

		createContents();

	}

	/**
	 * Create contents of the shell.
	 */
	protected void createContents() {
		setText("A DayTime server");
		setSize(399, 378);

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
